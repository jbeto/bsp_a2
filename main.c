// Author: Johannes Berger, Edgar Toll

#include <stdio.h>
#include <stdlib.h>

#include "fifo.h"
#include "threadHandler.h"

void fifoTest() {

    int var = -1;

    printf("empty: %d full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("push 1: %i\n", FIFOIn(1));
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("push 2: %i\n", FIFOIn(2));
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("pop: %i - erwartet: 1, ", FIFOOut(&var));
    printf("bekommen: %i\n", var);
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("push 3: %i\n", FIFOIn(3));
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("pop: %i - erwartet: 2, ", FIFOOut(&var));
    printf("bekommen: %i\n", var);
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
    printf("pop: %i - erwartet: 3, ", FIFOOut(&var));
    printf("bekommen: %i\n", var);
    printf("empty: %i full: %i elements: %i\n", FIFOIsEmpty(), FIFOIsFull(), FIFOElementsOnStack());
}

int main(int argc, char** argv) {

    //fifoTest();
    ThreadHandlerInit();

    return (EXIT_SUCCESS);
}

