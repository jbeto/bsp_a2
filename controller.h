// Author: Johannes Berger, Edgar Toll

#include "threadHandler.h"

#ifndef CONTROLLER_H
#define	CONTROLLER_H
#endif	/* CONTROLLER_H */

void* controller(void* param);
