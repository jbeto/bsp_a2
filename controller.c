// Author: Johannes Berger, Edgar Toll

#include "controller.h"

#include <stdio.h>
#include <ctype.h>

void* controller(void* param) {

    pthread_t* mainThread = (pthread_t*) param;

    int conRunning = 1;
    int pro1Running = 1;
    int pro2Running = 1;
    char command;

    while (1) {

        command = getc(stdin);
        command = tolower(command);

        switch (command) {
            case 'h':
                printf("\n\nProducer / Consumer System\n\n"
                        "Mögliche Eingaben: \n"
                        "h: Dieser Hilfetext wird angezeigt \n"
                        "1: Producer 1 wird an/aus geschaltet \n"
                        "2: Producer 2 wird an/aus geschaltet \n"
                        "c: Consumer wird an/aus geschaltet \n"
                        "q: Das Programm wird beendet\n\n");
                break;
            case 'c':
                if (conRunning) {
                    printf("Consumer: Aus\n");
                    pthread_mutex_lock(&mutexCon);
                    conRunning = 0;
                } else {
                    printf("Consumer: An\n");
                    pthread_mutex_unlock(&mutexCon);
                    conRunning = 1;
                }
                break;
            case '1':
                if (pro1Running) {
                    printf("Producer 1: Aus\n");
                    pthread_mutex_lock(&mutexPro1);
                    pro1Running = 0;
                } else {
                    printf("Producer 1: An\n");
                    pthread_mutex_unlock(&mutexPro1);
                    pro1Running = 1;
                }
                break;
            case '2':
                if (pro2Running) {
                    printf("Producer 2: Aus\n");
                    pthread_mutex_lock(&mutexPro2);
                    pro2Running = 0;
                } else {
                    printf("Producer 2: An\n");
                    pthread_mutex_unlock(&mutexPro2);
                    pro2Running = 1;
                }
                break;
            case 'q':
                printf("Das Programm wird beendet\n");
                pthread_kill(*mainThread, 9);
                break;
            default:
                break;
        }
    }
}
