// Author: Johannes Berger, Edgar Toll

#include "fifo.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct entry {
    int value;
    void *next;
} FIFOEntry;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

FIFOEntry *firstElement = NULL;
int currentElements = 0;

int FIFOElementsOnStack(void) {
    int returnVal;

    if (pthread_mutex_lock(&mutex) != 0) {
        printf("Mutex Lock Unsuccessful");
        return FIFO_ERROR_MUTEX_LOCK;
    }

    returnVal = currentElements;

    if (pthread_mutex_unlock(&mutex) != 0) {
        printf("Mutex Unlock Unsuccessful");
        return FIFO_ERROR_MUTEX_UNLOCK;
    }

    return returnVal;
}

int FIFOIsEmpty(void) {
    return FIFOElementsOnStack() == 0;
}

int FIFOIsEmptyUnsafe(void) {
    return currentElements == 0;
}

int FIFOIsFull(void) {
    return FIFOElementsOnStack() == FIFO_SIZE;
}

int FIFOIsFullUnsafe(void) {
    return currentElements == FIFO_SIZE;
}

int FIFOIn(int element) {
    if (pthread_mutex_lock(&mutex) != 0) {
        printf("Mutex Lock Unsuccessful");
        return FIFO_ERROR_MUTEX_LOCK;
    }

    if (FIFOIsFullUnsafe())
        return FIFO_ERROR_FULL;

    //Element erstellen
    FIFOEntry *newEntry = NULL;
    newEntry = malloc(sizeof (FIFOEntry));
    if (newEntry == NULL)
        return FIFO_ERROR_MALLOC_UNSUCCESSFUL;

    newEntry->next = NULL;
    newEntry->value = element;

    if (firstElement == NULL) {
        //neue Liste erstellen
        firstElement = newEntry;
    } else {
        //an Liste anhängen
        FIFOEntry *lastEntry = firstElement;
        while (lastEntry->next != NULL) {
            lastEntry = lastEntry->next;
        }

        lastEntry->next = newEntry;
    }

    currentElements++;

    if (pthread_mutex_unlock(&mutex) != 0) {
        printf("Mutex Unlock Unsuccessful");
        return FIFO_ERROR_MUTEX_UNLOCK;
    }

    return 0;
}

int FIFOOut(int* element) {
    if (pthread_mutex_lock(&mutex) != 0) {
        printf("Mutex Lock Unsuccessful");
        return FIFO_ERROR_MUTEX_LOCK;
    }

    if (FIFOIsEmptyUnsafe())
        return FIFO_ERROR_EMPTY;

    //Element extrahieren
    *element = firstElement->value;

    //Element aus Liste entfernen
    void* currentElement = firstElement;
    firstElement = firstElement->next;
    free(currentElement);

    currentElements--;

    if (pthread_mutex_unlock(&mutex) != 0) {
        printf("Mutex Unlock Unsuccessful");
        return FIFO_ERROR_MUTEX_UNLOCK;
    }

    return 0;
}
