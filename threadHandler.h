// Author: Johannes Berger, Edgar Toll

#include <pthread.h>

#ifndef THREADHANDLER_H
#define	THREADHANDLER_H

#endif	/* THREADHANDLER_H */

extern pthread_mutex_t mutexPro1;
extern pthread_mutex_t mutexPro2;
extern pthread_mutex_t mutexCon;

extern pthread_cond_t condConsumer;
extern pthread_cond_t condPro;

int ThreadHandlerInit(void);
