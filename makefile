# Makefile für BSP A2 - Threadprogrammierung

CC      = gcc
CFLAGS  = -Wall -g
LDFLAGS = -pthread
OBJS    = consumer.o controller.o fifo.o main.o producer.o threadHandler.o
SRC	= consumer.c controller.c fifo.c main.c producer.c threadHandler.c
PROG	= bsp2
DEPENDFILE = .depend

dep: $(SRC)
	$(CC) -MM $(SRC) > $(DEPENDFILE)

-include $(DEPENDFILE)

all: $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -rf $(OBJS)

fullclean:
	rm -rf $(PROG) $(OBJS)

