// Author: Johannes Berger, Edgar Toll

#ifndef FIFO_H
#define	FIFO_H

#define FIFO_SIZE 10
#define FIFO_ERROR_FULL 1
#define FIFO_ERROR_EMPTY 2
#define FIFO_ERROR_MALLOC_UNSUCCESSFUL 3
#define FIFO_ERROR_MUTEX_LOCK 10
#define FIFO_ERROR_MUTEX_UNLOCK 11

#endif	/* FIFO_H */

int FIFOElementsOnStack(void);

int FIFOIsEmpty(void);

int FIFOIsFull(void);

int FIFOIn(int element);

int FIFOOut(int* element);
