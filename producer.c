// Author: Johannes Berger, Edgar Toll

#include "producer.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "fifo.h"
#include "threadHandler.h"

void* producer(void* param) {

    pthread_mutex_t dummyProdMutex = PTHREAD_MUTEX_INITIALIZER;

    Attributes* attr = (Attributes*) param;

    printf("Producer Started\n");

    int i;

    for (i = attr->firstElement; i <= attr->lastElement; i++) {

        if (pthread_mutex_lock(attr->runMutex) != 0) {
            printf("Mutex Lock Unsuccessful");
        }
        if (pthread_mutex_unlock(attr->runMutex) != 0) {
            printf("Mutex Unlock Unsuccessful");
        }
        //printf("Producer should run\n");

        if (pthread_mutex_lock(&dummyProdMutex) != 0) {
            printf("Mutex Lock Unsuccessful");
        }

        //printf("Producer mutex locked\n");


        while (FIFOIsFull()) {
            pthread_cond_wait(&condPro, &dummyProdMutex);
        }

        //printf("Producer can write\n");


        if (FIFOIn(i) == FIFO_ERROR_MALLOC_UNSUCCESSFUL) {
            printf("FIFO_ERROR_MALLOC_UNSUCCESSFUL");
        }
        printf("Producer created: %c\n", (char) i);

        if (pthread_mutex_unlock(&dummyProdMutex) != 0) {
            printf("Mutex Unlock Unsuccessful");
        }
        if (pthread_cond_signal(&condConsumer) != 0) {
            printf("Cond_signal: Wakeup failed");
        }

        sleep(3);
    }
    return 0;
}
