// Author: Johannes Berger, Edgar Toll

#include "threadHandler.h"

#include <pthread.h>
#include <stdio.h>

#include "consumer.h"
#include "producer.h"
#include "controller.h"

// Mutexe um zu prüfen, ob ein Thread laufen soll oder nicht
pthread_mutex_t mutexPro1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexPro2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexCon = PTHREAD_MUTEX_INITIALIZER;

// Condition um einen Thread "aufzuwecken"
pthread_cond_t condConsumer = PTHREAD_COND_INITIALIZER;
pthread_cond_t condPro = PTHREAD_COND_INITIALIZER;

int ThreadHandlerInit(void) {

    pthread_t con;
    pthread_t prod1;
    pthread_t prod2;
    pthread_t conTroll;
    pthread_t main = pthread_self();

    Attributes attr1 = {'a', 'z', &mutexPro1};
    Attributes attr2 = {'A', 'Z', &mutexPro2};

    if (pthread_create(&con, 0, consumer, 0) != 0) {
        printf("Thread Creation Failed");
    }
    if (pthread_create(&prod1, 0, producer, &attr1) != 0) {
        printf("Thread Creation Failed");
    }
    if (pthread_create(&prod2, 0, producer, &attr2) != 0) {
        printf("Thread Creation Failed");
    }
    if (pthread_create(&conTroll, 0, controller, &main) != 0) {
        printf("Thread Creation Failed");
    }

    if (pthread_join(prod1, 0) != 0) {
        printf("Thread Joining Failed");
    }
    if (pthread_join(prod2, 0) != 0) {
        printf("Thread Creation Failed");
    }
    if (pthread_join(con, 0) != 0) {
        printf("Thread Creation Failed");
    }

    pthread_cond_destroy(&condConsumer);
    pthread_cond_destroy(&condPro);
    pthread_mutex_destroy(&mutexCon);
    pthread_mutex_destroy(&mutexPro1);
    pthread_mutex_destroy(&mutexPro2);

    return 0;
}
