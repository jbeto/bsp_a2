// Author: Johannes Berger, Edgar Toll

#include "consumer.h"

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include "fifo.h"
#include "threadHandler.h"

void* consumer(void* useless) {

    pthread_mutex_t dummyConsMutex = PTHREAD_MUTEX_INITIALIZER;

    printf("Consumer Started\n");

    int value;

    while (1) {

        if (pthread_mutex_lock(&mutexCon) != 0) {
            printf("Mutex Lock Unsuccessful");
        }
        if (pthread_mutex_unlock(&mutexCon) != 0) {
            printf("Mutex Unlock Unsuccessful");
        }

        if (pthread_mutex_lock(&dummyConsMutex) != 0) {
            printf("Mutex Lock Unsuccessful");
        }

        while (FIFOIsEmpty())
            pthread_cond_wait(&condConsumer, &dummyConsMutex);

        FIFOOut(&value);
        printf("Consumer got value: %c\n", (char) value);

        if (pthread_mutex_unlock(&dummyConsMutex) != 0) {
            printf("Mutex Unlock Unsuccessful");
        }
        if (pthread_cond_broadcast(&condPro) != 0) {
            printf("Cond_signal: Wakeup failed");
        }

        sleep(2);
    }
}
