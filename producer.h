// Author: Johannes Berger, Edgar Toll

#include <pthread.h>

#ifndef PRODUCER_H
#define	PRODUCER_H

//typedef struct range {
//    int firstElement;
//    int lastElement;
//} Range;

typedef struct attr {
    int firstElement;
    int lastElement;
    pthread_mutex_t* runMutex;
} Attributes;

#endif	/* PRODUCER_H */

void* producer(void* param);
